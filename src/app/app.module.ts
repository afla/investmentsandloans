import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { ClientsCrudComponent } from './components/clients-crud/clients-crud.component';
import { InvestmentsCrudComponent } from './components/investments-crud/investments-crud.component';
import { LoansCrudComponent } from './components/loans-crud/loans-crud.component';

import { RepositoryService } from './services/repository.service';
import { ClientsListComponent } from './components/clients-list/clients-list.component';
import { InvestmentsListComponent } from './components/investments-list/investments-list.component';
import { LoansListComponent } from './components/loans-list/loans-list.component';
import { ClientEditComponent } from './components/client-edit/client-edit.component'
import { LoanEditComponent } from './components/loan-edit/loan-edit.component'
import { InvestmentEditComponent } from './components/investment-edit/investment-edit.component'

const routes: Routes = [ 
  { path: '', redirectTo: 'clients-crud', pathMatch: 'full' }, 
  { path: 'clients-crud', component: ClientsCrudComponent }, 
  { path: 'investments-crud', component: InvestmentsCrudComponent },
  { path: 'loans-crud', component: LoansCrudComponent }
]; 

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    ClientsCrudComponent,
    InvestmentsCrudComponent,
    LoansCrudComponent,
    ClientsListComponent,
    ClientEditComponent,
    InvestmentsListComponent,
    InvestmentEditComponent,
    LoansListComponent,
    LoanEditComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    RepositoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
