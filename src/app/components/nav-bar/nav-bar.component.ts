import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nav-bar-component',
  templateUrl: './nav-bar.component.html',
  styles: [`
    nav {
        background-color: #dcdcdc;
        padding: 0px;
        margin: 0px;
    }
  `]
})
export class NavBarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
