import {Component} from "@angular/core";
import {ClientModel} from "../../model/client.model";
import {RepositoryService} from "../../services/repository.service";

@Component({
  selector: 'clients-crud-component',
  templateUrl: 'clients-crud.component.html'
})
export class ClientsCrudComponent {
  public selectedClient;

  constructor(private repositoryService: RepositoryService) {}

  updateClient(client: ClientModel) {
    this.selectedClient.name = client.name;
    this.selectedClient.phone = client.phone;
  }

  createClient(client: ClientModel) {
    this.repositoryService.createClient(client, this);
  }

}