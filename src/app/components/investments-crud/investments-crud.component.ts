import {Component} from "@angular/core";
import {InvestmentModel} from "../../model/investment.model";
import {RepositoryService} from "../../services/repository.service";

@Component({
    selector: 'investments-crud-component',
    templateUrl: 'investments-crud.component.html'
})
export class InvestmentsCrudComponent {
    public selectedInvestment;

    constructor(private repositoryService: RepositoryService) {
    }

    updateInvestment(investment: InvestmentModel) {
      this.selectedInvestment.investedAmount = investment.investedAmount;
      this.selectedInvestment.returnedAmount = investment.returnedAmount;
      this.selectedInvestment.investedDate = investment.investedDate;
      this.selectedInvestment.returnedDate = investment.returnedDate;
      this.selectedInvestment.clientId = investment.clientId;
    }

    createInvestment(investment: InvestmentModel) {
        this.repositoryService.createInvestment(investment, this);
    }

}