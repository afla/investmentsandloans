import { InvestmentsAndLoans2Page } from './app.po';

describe('investments-and-loans2 App', function() {
  let page: InvestmentsAndLoans2Page;

  beforeEach(() => {
    page = new InvestmentsAndLoans2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
